<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanyDatasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanyDatasTable Test Case
 */
class CompanyDatasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanyDatasTable
     */
    public $CompanyDatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CompanyDatas',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CompanyDatas') ? [] : ['className' => CompanyDatasTable::class];
        $this->CompanyDatas = TableRegistry::getTableLocator()->get('CompanyDatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyDatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
