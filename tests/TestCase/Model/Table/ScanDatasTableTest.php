<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ScanDatasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScanDatasTable Test Case
 */
class ScanDatasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ScanDatasTable
     */
    public $ScanDatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ScanDatas',
        'app.Registrants',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ScanDatas') ? [] : ['className' => ScanDatasTable::class];
        $this->ScanDatas = TableRegistry::getTableLocator()->get('ScanDatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ScanDatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
