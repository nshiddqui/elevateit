<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AppInfosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AppInfosTable Test Case
 */
class AppInfosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AppInfosTable
     */
    public $AppInfos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AppInfos',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AppInfos') ? [] : ['className' => AppInfosTable::class];
        $this->AppInfos = TableRegistry::getTableLocator()->get('AppInfos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AppInfos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
