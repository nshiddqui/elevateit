<?php
$this->assign('title', 'App Info Management');
?>
<?= $this->html->css('app_infos', ['block' => true]) ?>
<?= $this->html->script('app_infos', ['block' => true]) ?>
<?= $this->Form->create($appInfo, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Change App Info') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="logo-image">
                        <?= $this->Html->image((!empty($appInfo['logo_image']) ? $appInfo['logo_image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Image For App Logo</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('logo_image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('company_name');
                    echo $this->Form->control('tag_line');
                    echo $this->Form->control('privacy_policy');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>