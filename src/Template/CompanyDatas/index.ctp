<?php
$this->assign('title', 'Company Data');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Company Data Listing</h3>
        <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('CompanyDatas') ?>
    </div>
</div>