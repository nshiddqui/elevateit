<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        h($result->company_name),
        h($result->activation_code),
        h($result->created),
        $this->Html->link($this->Html->image('edit.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'edit', $result->id], ['escape' => false,]) .
        $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->title), 'escape' => false])
    ]);
}
echo $this->DataTables->response();
