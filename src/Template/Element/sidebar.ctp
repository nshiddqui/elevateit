<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <?= $this->Html->image('user.png?' . microtime(true), ['class' => 'img-circle', 'alt' => 'User Image']) ?>
        </div>
        <div class="pull-left info">
            <p><?= $authUser['name'] ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><?= $this->Html->link('<i class="fa fa-users"></i><span>User Management</span>', ['controller' => 'users'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-qrcode"></i><span>Scan Data Management</span>', ['controller' => 'scanDatas'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-bell"></i><span>Notification Management</span>', ['controller' => 'notifications'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-industry"></i><span>Company Data Management</span>', ['controller' => 'companyDatas'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-registered"></i><span>Registrants Data Management</span>', ['controller' => 'registrants'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-rocket"></i><span>App Info Management</span>', ['controller' => 'appInfos'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-user"></i><span>Admin Management</span>', ['controller' => 'Admins', 'action' => 'chnageProfile'], ['escape' => false]) ?></li>
    </ul>
</section>