<!-- Logo -->
<a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b><?= $this->Html->image('/favicon.ico') ?></b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><?= $this->Html->image('logo.png') ?></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Logout -->
            <li>
                <?= $this->Form->postLink('Logout <i class="fa fa-sign-out"></i>', ['controller' => 'admins', 'action' => 'logout'], ['escape' => false, 'confirm' => 'Are you sure want to logout?']) ?>
            </li>
        </ul>
    </div>
</nav>