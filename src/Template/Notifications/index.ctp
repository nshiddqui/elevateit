<?php
$this->assign('title', 'Notification Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Notification Listing</h3>
        <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Notifications') ?>
    </div>
</div>