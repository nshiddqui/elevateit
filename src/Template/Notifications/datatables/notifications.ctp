<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        h(!empty($result['user']) ? $result['user']->email : 'All Users'),
        h($result->title),
        h($result->detail),
        (!empty($result->link) ? $this->Html->link('<i class="fa fa-eye"></i>', $result->link, ['fancybox' => true, 'escape' => false]) : ''),
        h($result->created),
        $this->Html->link($this->Html->image('edit.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'edit', $result->id], ['escape' => false,]) .
        $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->title), 'escape' => false])
    ]);
}
echo $this->DataTables->response();
