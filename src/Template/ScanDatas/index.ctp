<?php
$this->assign('title', 'Scan Management');
?>
<?= $this->html->script('scan_data', ['block' => true]) ?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Scan Listing</h3>
        <?= $this->Html->link('<i class="fa fa-download"></i>', ['action' => 'export'], ['escape' => false, 'class' => 'btn btn-info pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <?= $this->DataTables->render('ScanDatas') ?>
        </div>
    </div>
</div>