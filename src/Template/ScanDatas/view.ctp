<?php
$this->assign('title', 'View User Profile');
?>
<div class="box-body">
    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href='javascript:void(0)'><strong>Scan ID</strong><span class="pull-right"><?= $scanData['id'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Registrant ID</strong><span class="pull-right"><?= $scanData['registrant_id'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Name</strong><span class="pull-right"><?= $scanData['name'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Title</strong><span class="pull-right"><?= $scanData['title'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Company</strong><span class="pull-right"><?= $scanData['company'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Phone No</strong><span class="pull-right"><?= $scanData['phone'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Address</strong><span class="pull-right"><?= $scanData['address'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Email</strong><span class="pull-right"><?= $scanData['email'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Scan By</strong><span class="pull-right"><?= $scanData['user']['email'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Registration Date & Time</strong><span class="pull-right"><?= $scanData['created'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Notes</strong><span class="pull-right"><?= $scanData['notes'] ?></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>