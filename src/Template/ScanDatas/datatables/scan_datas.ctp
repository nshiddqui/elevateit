<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        h($result->registrant_id),
        h($result->name),
        h($result->title),
        h($result->company),
        h($result->phone),
        !empty($result['user']) ? $this->Html->link($result['user']->full_name, ['controller' => 'users', 'action' => 'view', $result['user']->id]) : 'No Users',
        h($result->created),
        $this->Html->link($this->Html->image('view.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'view', $result->id], ['escape' => false]) .
        $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $result->id], ['confirm' => 'Are you sure to delete this record?', 'escape' => false])
    ]);
}
echo $this->DataTables->response();
