<?php
$this->assign('title', 'User Management');
?>
<?= $this->html->css('user', ['block' => true]) ?>
<?= $this->html->script('user', ['block' => true]) ?>
<?= $this->Form->create($user, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add New User') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image('not-found.png', ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'user.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For User</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('first_name');
                    echo $this->Form->control('last_name');
                    echo $this->Form->control('phone');
                    echo $this->Form->control('email');
                    echo $this->Form->control('password');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>