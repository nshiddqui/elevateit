<?php
$this->assign('title', 'User Management');
?>
<?= $this->html->css('user', ['block' => true]) ?>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua br-20">
            <div class="inner">
                <h3><?= $total_users ?></h3>
                <p>Total Users</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green br-20">
            <div class="inner">
                <h3><?= $total_registrants ?></h3>
                <p>Total Registrants</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow br-20">
            <div class="inner">
                <h3><?= $total_scans ?></h3>
                <p>Total Scans</p>
            </div>
            <div class="icon">
                <i class="ion ion-qr-scanner"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red br-20">
            <div class="inner">
                <h3><?= $total_scan_today ?></h3>
                <p>Scans Done Today</p>
            </div>
            <div class="icon">
                <i class="ion ion-qr-scanner"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Users</h3>
        <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Users') ?>
    </div>
</div>