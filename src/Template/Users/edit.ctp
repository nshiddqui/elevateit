<?php
$this->assign('title', 'User Management');
?>
<?= $this->html->css('user', ['block' => true]) ?>
<?= $this->html->script('user', ['block' => true]) ?>
<?= $this->Form->create($user, ['type' => 'file', 'id' => 'user']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Edit User Profile') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image((!empty($user['image']) ? $user['image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Profile Image For User</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('first_name');
                    echo $this->Form->control('last_name');
                    echo $this->Form->control('phone');
                    echo $this->Form->control('email');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>