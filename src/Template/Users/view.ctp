<?php
$this->assign('title', 'View User Profile');
?>
<div class="box-body">
    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active text-center">
                    <?= $this->Html->image((!empty($user['image']) ? $user['image'] : 'not-found.png'), ['class' => 'img-circle', 'style' => 'height:110px']) ?>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href='javascript:void(0)'><strong>User ID</strong><span class="pull-right"><?= $user['id'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>First Name</strong><span class="pull-right"><?= $user['first_name'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Last Name</strong><span class="pull-right"><?= $user['last_name'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Email</strong><span class="pull-right"><?= $user['email'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Phone No.</strong><span class="pull-right"><?= $user['phone'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Device Info</strong><span class="pull-right"><?= $user['device_info'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Total Scans</strong><span class="pull-right"><?= 0 ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Registration Date & Time</strong><span class="pull-right"><?= $user['created'] ?></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>