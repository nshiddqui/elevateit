<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        $this->Html->image((!empty($result->image) ? $result->image : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle']),
        h($result->first_name),
        h($result->last_name),
        h($result->email),
        h($result->phone),
        $this->Html->link(count($result['scan_datas']), ['controller' => 'ScanDatas', 'action' => 'index', '?' => ['user_id' => $result->id]]),
        $this->Html->link($this->Html->image('view.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'view', $result->id], ['escape' => false]) . '&nbsp;' .
        $this->Html->link($this->Html->image('edit.png', ['style' => 'height: 20px;']), ['action' => 'edit', $result->id], ['escape' => false]) . '&nbsp;' .
        $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;']), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->email), 'escape' => false])
    ]);
}
echo $this->DataTables->response();
