<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        h($result->displayId),
        h($result->first_name),
        h($result->last_name),
        h($result->email),
        h($result->phone_number),
        h($result->company),
        h($result->title),
        h($result->street_address),
        h($result->city),
        h($result->state),
        h($result->country),
        h($result->created),
    ]);
}
echo $this->DataTables->response();
