<?php
$this->assign('title', 'Registrants Data Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Registrants</h3>
        <?= $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'deleteAll'], ['confirm' => __('Are you sure want to delete all registrants? This will delete all data'), 'escape' => false, 'class' => 'pull-right', 'title' => 'Delete All']) ?>&nbsp;
        <?php if ($refereshRequired) { ?>
            <?= $this->Html->link('<i class="fa fa-refresh"></i>', ['action' => 'referesh'], ['escape' => false, 'class' => 'btn btn-info pull-right']) ?>
        <?php } ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
        <?= $this->DataTables->render('Registrants') ?>
    </div>
</div>