<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppInfos Model
 *
 * @method \App\Model\Entity\AppInfo get($primaryKey, $options = [])
 * @method \App\Model\Entity\AppInfo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AppInfo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppInfo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AppInfo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AppInfo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AppInfosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('app_infos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('logo_image')
            ->maxLength('logo_image', 255)
            ->requirePresence('logo_image', 'create')
            ->notEmptyString('logo_image');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 255)
            ->requirePresence('company_name', 'create')
            ->notEmptyString('company_name');

        $validator
            ->scalar('tag_line')
            ->requirePresence('tag_line', 'create')
            ->notEmptyString('tag_line');

        $validator
            ->scalar('privacy_policy')
            ->maxLength('privacy_policy', 1024)
            ->requirePresence('privacy_policy', 'create')
            ->notEmptyString('privacy_policy');

        return $validator;
    }
}
