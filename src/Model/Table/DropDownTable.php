<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class DropDownTable extends Table {

    public $media_type = [
        'Image',
        'Video',
        'Audio'
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable(false);
    }

    public function getMediaType($key = null) {
        if (is_null($key)) {
            return $this->media_type;
        } else {
            return $this->media_type[$key];
        }
    }

    public function addMediaType($key, $value) {
        $this->media_type[$key] = $value;
    }

    public function removeMediaType($value) {
        if ($key = array_search($value, $this->media_type)) {
            unset($this->media_type[$key]);
        }
    }

}
