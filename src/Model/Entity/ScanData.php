<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ScanData Entity
 *
 * @property int $id
 * @property string $registrant_id
 * @property string $name
 * @property string $title
 * @property string $company
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $notes
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Registrant $registrant
 * @property \App\Model\Entity\User $user
 */
class ScanData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'registrant_id' => true,
        'name' => true,
        'title' => true,
        'company' => true,
        'phone' => true,
        'address' => true,
        'email' => true,
        'notes' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'registrant' => true,
        'user' => true,
    ];
}
