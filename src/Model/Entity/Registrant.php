<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Registrant Entity
 *
 * @property int $id
 * @property string $displayId
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $email
 * @property string|null $phone_number
 * @property string|null $company
 * @property string|null $title
 * @property string|null $street_address
 * @property string|null $city
 * @property string|null $state
 * @property string|null $country
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Registrant extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'displayId' => true,
        'first_name' => true,
        'last_name' => true,
        'email' => true,
        'phone_number' => true,
        'company' => true,
        'title' => true,
        'street_address' => true,
        'city' => true,
        'state' => true,
        'country' => true,
        'created' => true,
        'modified' => true,
    ];
}
