<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CompanyData Entity
 *
 * @property int $id
 * @property string $company_name
 * @property string $activation_code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class CompanyData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_name' => true,
        'activation_code' => true,
        'created' => true,
        'modified' => true,
    ];
}
