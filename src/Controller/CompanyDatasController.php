<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * CompanyDatas Controller
 *
 * @property \App\Model\Table\CompanyDatasTable $CompanyDatas
 *
 * @method \App\Model\Entity\CompanyData[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompanyDatasController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('CompanyDatas')
                ->column('CompanyDatas.id', ['label' => '#'])
                ->column('CompanyDatas.company_name', ['label' => 'Company Name'])
                ->column('CompanyDatas.activation_code', ['label' => 'Activation Code'])
                ->column('CompanyDatas.created', ['label' => 'Date & Time'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->CompanyDatas);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('CompanyDatas');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $companyData = $this->CompanyDatas->newEntity();
        if ($this->request->is('post')) {
            $companyData = $this->CompanyDatas->patchEntity($companyData, $this->request->getData());
            if ($this->CompanyDatas->save($companyData)) {
                $this->Flash->success(__('The company data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company data could not be saved. Please, try again.'));
        }
        $this->set(compact('companyData'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Company Data id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $companyData = $this->CompanyDatas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $companyData = $this->CompanyDatas->patchEntity($companyData, $this->request->getData());
            if ($this->CompanyDatas->save($companyData)) {
                $this->Flash->success(__('The company data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company data could not be saved. Please, try again.'));
        }
        $this->set(compact('companyData'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Company Data id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $companyData = $this->CompanyDatas->get($id);
        if ($this->CompanyDatas->delete($companyData)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
