<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\MailerAwareTrait;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 *
 * @method \App\Model\Entity\Admin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow(['logout', 'login', 'forgot']);
        parent::beforeFilter($event);
    }

    public function login() {
        $this->viewBuilder()->setLayout(false);
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['status'] === true) {
                    $this->Auth->setUser($user);
                    if ($this->request->is('api')) {
                        $this->Api->login($user);
                    }
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('This user not activated, please contact our administrator.'));
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function chnageProfile() {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        $admin = $this->Admins->get(1, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $admin = $this->Admins->patchEntity($admin, $this->getAdminData());
            if ($this->Admins->save($admin)) {
                $this->Auth->setUser($admin->toArray());
                $this->Flash->success(__('The profile has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The profile could not be saved. Please, try again.'));
        }
        $this->set(compact('admin'));
    }

    private function getAdminData() {
        $data = $this->request->getData();
        if (empty($data['password'])) {
            unset($data['password']);
        }
        if (!empty($data['thumbnail']['tmp_name'])) {
            $this->loadComponent('Fileupload');
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'img/', 'file_name' => 'user.png', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'overwrite' => true]);
            $this->Fileupload->upload('thumbnail');
        }
        unset($data['thumbnail']);
        return $data;
    }

    
    use MailerAwareTrait;
    public function forgot() {
        if ($this->request->is('post')) {
            $email = $this->request->getData('email');
            if (!empty($email)) {
                $userData = $this->Admins->findByEmail($email);
                if ($userData->count()) {
                    $userDetails = $userData->first();
                    $userDetails->token = $this->Admins->tokenUpdate($email);
                    $emailObj = $this->getMailer('User');
                    if ($emailObj->send('resetPassword', [$userDetails])) {
                        $this->Flash->success(__('New password has been sent on your email id'));
                        $this->redirect($this->referer());
                    } else {
                        $this->Flash->error(__('Reset Password could not be sent on your email. Please, try again.'));
                    }
                } else {
                    $this->Flash->error(__('Provided Username not exists.'));
                }
            } else {
                $this->Flash->error(__('Please enter Username.'));
            }
        }
        $this->viewBuilder()->setLayout(false);
    }

}
