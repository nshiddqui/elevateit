<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * ScanDatas Controller
 *
 * @property \App\Model\Table\ScanDatasTable $ScanDatas
 *
 * @method \App\Model\Entity\ScanData[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScanDatasController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('ScanDatas')
                ->queryOptions([
                    'contain' => ['Users']
                ])
                ->databaseColumn('Users.id')
                ->databaseColumn('Users.last_name', true)
                ->column('ScanDatas.id', ['label' => '#'])
                ->column('ScanDatas.registrant_id', ['label' => 'Registrant ID'])
                ->column('ScanDatas.name', ['label' => 'Name'])
                ->column('ScanDatas.title', ['label' => 'Title'])
                ->column('ScanDatas.company', ['label' => 'Company'])
                ->column('ScanDatas.phone', ['label' => 'Phone No', 'width' => '120px'])
                ->column('Users.first_name', ['label' => 'Scan By'])
                ->column('ScanDatas.created', ['label' => 'Date & Time'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->ScanDatas);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('ScanDatas');
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $scanData = $this->ScanDatas->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('scanData', $scanData);
    }

    /**
     * Delete method
     *
     * @param string|null $id Scan Data id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $scanData = $this->ScanDatas->get($id);
        if ($this->ScanDatas->delete($scanData)) {
            $this->Flash->success(__('The scan has been deleted.'));
        } else {
            $this->Flash->error(__('The scan could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function export() {
        $datas = $this->ScanDatas->find('all', [
            'contain' => ['Users']
        ]);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Scan ID');
        $sheet->setCellValue('B1', 'Registrant ID');
        $sheet->setCellValue('C1', 'Name');
        $sheet->setCellValue('D1', 'Title');
        $sheet->setCellValue('E1', 'Company');
        $sheet->setCellValue('F1', 'Phone No');
        $sheet->setCellValue('G1', 'Address');
        $sheet->setCellValue('H1', 'Email');
        $sheet->setCellValue('I1', 'Scan By');
        $sheet->setCellValue('J1', 'Registration Date & Time');
        $sheet->setCellValue('K1', 'Notes');

        $i = 2;
        foreach ($datas as $d) {
            $sheet->setCellValue('A' . $i, $d->id);
            $sheet->setCellValue('B' . $i, $d->registrant_id);
            $sheet->setCellValue('C' . $i, $d->name);
            $sheet->setCellValue('D' . $i, $d->title);
            $sheet->setCellValue('E' . $i, $d->company);
            $sheet->setCellValue('F' . $i, $d->phone);
            $sheet->setCellValue('G' . $i, $d->address);
            $sheet->setCellValue('H' . $i, $d->email);
            $sheet->setCellValue('I' . $i, $d['user']->email);
            $sheet->setCellValue('J' . $i, $d->created);
            $sheet->setCellValue('L' . $i, $d->notes);
            $i = $i + 1;
        }
        $writer = new Xlsx($spreadsheet);
        $filePath = WWW_ROOT . 'download/' . time() . '.xlsx';
        $writer->save($filePath);
        return $this->response
                        ->withHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                        ->withHeader('Content-Disposition', 'attachment;')
                        ->withHeader('Cache-Control', 'max-age=0')
                        ->withHeader('Cache-Control', 'max-age=1')
                        ->withFile($filePath, ['download' => true]);
    }

}
