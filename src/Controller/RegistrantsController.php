<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Client;

/**
 * Registrants Controller
 *
 * @property \App\Model\Table\RegistrantsTable $Registrants
 *
 * @method \App\Model\Entity\Registrant[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RegistrantsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Registrants')
                ->column('Registrants.id', ['label' => '#'])
                ->column('Registrants.displayId', ['label' => 'Display ID'])
                ->column('Registrants.first_name', ['label' => 'First Name'])
                ->column('Registrants.last_name', ['label' => 'Last Name'])
                ->column('Registrants.email', ['label' => 'Email'])
                ->column('Registrants.phone_number', ['label' => 'Phone Number'])
                ->column('Registrants.company', ['label' => 'Company'])
                ->column('Registrants.title', ['label' => 'Title'])
                ->column('Registrants.street_address', ['label' => 'Street Address'])
                ->column('Registrants.city', ['label' => 'City'])
                ->column('Registrants.state', ['label' => 'State'])
                ->column('Registrants.country', ['label' => 'Country'])
                ->column('Registrants.created', ['label' => 'Date & Time']);
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow('runCron');
        parent::beforeFilter($event);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Registrants);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Registrants');
            $this->set('refereshRequired', $this->checkRefereshRequired());
        }
    }

    public function referesh() {
        if ($this->uploadData($this->Auth->user('registrant_page'))) {
            $this->Flash->success(__('The registrant has been referesh.'));
        }
        $this->redirect($this->referer());
    }

    private function uploadData($startingAfter, $exitPage = 0) {
        if ($exitPage === 1) {
            $this->updatePageNumber($startingAfter);
            return true;
        }
        $jsonData = $this->getApiData($startingAfter);
        if (isset($jsonData['data']) && !empty($jsonData['data'])) {
            $header = [
                'first_name' => 'name.first',
                'last_name' => 'name.last',
                'company' => 'company',
                'title' => 'title',
                'email' => 'email',
                'phone_number' => 'phone',
                'state' => 'address.state',
                'street_address' => 'address.street1',
                'city' => 'address.city',
                'country' => 'address.country'
            ];
            foreach ($jsonData['data'] as $data) {
                $tmpData = [
                    'id' => $data['id'],
                    'displayId' => $data['displayId'],
                    'created' => date('Y-m-d h:i:s', strtotime($data['dateCreated'])),
                    'modified' => date('Y-m-d h:i:s', strtotime($data['dateUpdated']))
                ];
                foreach ($data['fieldData'] as $fieldData) {
                    $key = array_search($fieldData['path'], $header);
                    if ($key !== false) {
                        $tmpData[$key] = $fieldData['value'];
                    }
                }
                $isAlreadyExist = $this->Registrants->find('all', [
                    'conditions' => [
                        'id' => $tmpData['id']
                    ]
                ]);

                if ($isAlreadyExist->count() === 0) {
                    $registrant = $this->Registrants->newEntity();
                    $registrant = $this->Registrants->patchEntity($registrant, $tmpData);
                    $registrant->id = $tmpData['id'];
                    if (!$this->Registrants->save($registrant)) {
                        foreach ($registrant->errors() as $e => $ev) {
                            $this->Flash->error(__("Error at registrant number: " . $registrant->id . " in column " . $e . " with value " . $tmpData[$e]));
                            return false;
                        }
                    }
                }
            }
        }
        if ($jsonData['hasMore'] && $jsonData['hasMore'] === true) {
            return $this->uploadData($jsonData['startingAfter'], $exitPage + 1);
        } else {
            $this->updatePageNumber($startingAfter);
        }
        return true;
    }

    private function updatePageNumber($startingAfter) {
        $this->loadModel('Admins');
        $admiData = $this->Admins->get($this->Auth->user('id'));
        $admiData->registrant_page = $startingAfter;
        if ($this->Admins->save($admiData)) {
            $this->Auth->setUser($admiData->toArray());
        }
    }

    /**
     * deleteAll method
     *
     * @param null
     * @return \Cake\Http\Response|null Redirects to index.
     */
    public function deleteAll() {
        $this->Registrants->deleteAll([1 => 1]);
        $this->updatePageNumber(0);
        $this->Flash->success(__('All registrant has been deleted.'));
        return $this->redirect($this->referer());
    }

    private function getApiData($startingAfter) {
        $client = new Client();
        $response = $client->get('https://api.webconnex.com/v2/public/search/registrants', [
            'startingAfter' => $startingAfter
                ], ['headers' => ['apiKey' => '344623393c0749a499c482acab112780']]);
        return $response->getJson();
    }

    private function checkRefereshRequired() {
        $totalRecord = $this->Registrants->find()->count();
        $apiRecord = $this->getApiData($this->Auth->user('registrant_page'))['totalResults'];
        if ($apiRecord > $totalRecord) {
            return true;
        } else {
            return false;
        }
    }

    public function runCron() {
        $this->loadModel('Admins');
        $admiData = $this->Admins->get(1);
        $this->Auth->setUser($admiData->toArray());
        $this->uploadData($admiData->registrant_page);
        $this->Auth->logout();
        die;
    }

}
