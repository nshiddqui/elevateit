<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * AppInfos Controller
 *
 * @property \App\Model\Table\AppInfosTable $AppInfos
 *
 * @method \App\Model\Entity\AppInfo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppInfosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        $appInfo = $this->AppInfos->get(1, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $appInfo = $this->AppInfos->patchEntity($appInfo, $this->getAppInfoData());
            if ($this->AppInfos->save($appInfo)) {
                $this->Flash->success(__('The app info has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The app info could not be saved. Please, try again.'));
        }
        $this->set(compact('appInfo'));
    }

    public function getAppInfoData() {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        if (!empty($data['logo_image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('logo_image');
            $data['logo_image'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
        } else {
            unset($data['logo_image']);
        }
        return $data;
    }

}
