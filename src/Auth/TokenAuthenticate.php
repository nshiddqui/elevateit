<?php

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\UnauthorizedException;

class TokenAuthenticate extends BaseAuthenticate
{

    public $settings = [
        'fields' => [
            'token' => 'token'
        ],
        'userModel' => 'Admins',
        'userFields' => null,
        'scope' => ['status' => 1],
        'recursive' => 0,
        'contain' => null
    ];

    public function __construct(\Cake\Controller\ComponentRegistry $registry, array $config = array())
    {
        $this->settings = array_merge($this->settings, $config);
    }

    public function authenticate(Request $request, Response $response)
    {
        return $this->getUser($request);
    }

    public function getUser(Request $request)
    {
        if ($token = $request->getHeaderLine('Auth')) {
            $userModel = $this->settings['userModel'];
            list(, $model) = pluginSplit($userModel);
            $fields = $this->settings['fields'];

            $conditions = [
                $model . '.' . $fields['token'] => $token
            ];

            if (!empty($this->settings['scope'])) {
                $conditions = array_merge($conditions, $this->settings['scope']);
            }

            $userFields = $this->settings['userFields'];
            if ($token !== null && $userFields !== null) {
                $userFields[] = $model . '.' . $fields['token'];
            }

            $result = TableRegistry::get($userModel)->find('all', [
                'conditions' => $conditions,
                'recursive' => $this->settings['recursive'],
                'fields' => $userFields,
                'contain' => $this->settings['contain'],
            ])->first();
            if (empty($result)) {
                return false;
            }
            return json_decode(json_encode($result), true);
            // use web login
        } elseif ($request->getSession()->check('Auth.User')) {
            return $request->getSession()->read('Auth.User');
        }

        return false;
    }

    public function unauthenticated(Request $request, Response $response)
    {
        $message = $request->getHeaderLine('Auth') ? 'Invalid Token' : 'Token Not Set';
        $code = $request->getHeaderLine('Auth') ? 401 : 401;

        throw new UnauthorizedException($message, $code);
    }
}
