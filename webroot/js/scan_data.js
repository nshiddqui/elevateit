$(document).ready(function () {
    $('#users').on('change', function () {
        var val = $(this).val();
        var url = new URL(window.location.href);
        url.searchParams.set('user_id', val);
        window.location.href = url.href;
    });
});